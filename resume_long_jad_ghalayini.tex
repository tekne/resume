% !TEX program = xelatex

\documentclass[a4paper,12pt]{article}

%A Few Useful Packages
\usepackage{marvosym}
\usepackage{fontspec} 					%for loading fonts
\usepackage{xunicode,xltxtra,url,parskip} 	%other packages for formatting
\usepackage{enumitem}
\usepackage{booktabs}
\RequirePackage{color,graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[big]{layaureo} 				%better formatting of the A4 page
% an alternative to Layaureo can be ** \usepackage{fullpage} **
\usepackage{supertabular} 				%for Grades
\usepackage{titlesec}					%custom \section

%Setup hyperref package, and colours for links
\usepackage{hyperref}
\definecolor{linkcolour}{rgb}{0,0.2,0.6}
\hypersetup{colorlinks,breaklinks,urlcolor=linkcolour, linkcolor=linkcolour}

%FONTS
\defaultfontfeatures{Mapping=tex-text}
%\setmainfont[SmallCapsFont = Fontin SmallCaps]{Fontin}
%%% modified for Karol Kozioł for ShareLaTeX use
\setmainfont[
SmallCapsFont = Fontin-SmallCaps.otf,
BoldFont = Fontin-Bold.otf,
ItalicFont = Fontin-Italic.otf
]
{Fontin.otf}
%%%

%CV Sections inspired by:
%http://stefano.italians.nl/archives/26
\titleformat{\section}{\Large\scshape\raggedright}{}{0em}{}[\titlerule]
\titlespacing{\section}{0pt}{3pt}{3pt}
%Tweak a bit the top margin
%\addtolength{\voffset}{-1.3cm}

%--------------------BEGIN DOCUMENT----------------------
\begin{document}


\pagestyle{empty} % non-numbered pages

\font\fb=''[cmr10]'' %for use with \LaTeX command

%--------------------TITLE-------------
\par{\centering
	{\Huge Jad Ghalayini
	}\bigskip\par}

%--------------------SECTIONS-----------------------------------
%Section: Personal Data
\begin{center}
	+44 7429 510136
	\quad --- \quad
	\href{mailto:jeg74@cl.cam.ac.uk}{jeg74@cl.cam.ac.uk}
	\quad --- \quad
	\href{https://gitlab.com/tekne}{gitlab.com/tekne}
\end{center}




%Section: Education
\section{Education}
\begin{tabular*}{\textwidth}{l @{\extracolsep{\fill}} r}
	\textbf{University of Cambridge: PhD} 
	& \textsc {October} 2021 - \textsc{Present} 
	\\ Computer Science
	\\ \textbf{Supervisor:} Dr Neel Krishnaswami
	\\
	\\
	\textbf{University of Oxford: Master of Science} 
	& \textsc {October} 2020 - \textsc{September} 2021 
	\\ Computer Science
	\\ \textbf{Thesis:} \textit{Dependent Types with Borrowing}
	\\ \multicolumn{2}{l}{(PDF version: \url{https://isotope-project.gitlab.io/msc-thesis/thesis.pdf})}
	\\ \textbf{USM:} 85/100 (Distinction)
	\\
	\\ \textbf{University of Toronto: Honors Bachelor of Science} 
	& \textsc{September} 2017 - \textsc{April} 2020
	\\ Computer Science Specialist & 
	\\ Mathematics Specialist &
	\\ \textbf{GPA:} 3.97/4.0 (High Distinction) &
	\\
\end{tabular*}

\textsc{Research interests:} optimizing compilers, software verification, computer architecture, dependent types, linear/substructrual types, high-performance computing

\textsc{Technologies:} Rust, C, C++, Python, PyTorch, Tensorflow, Verilog, Haskell, Lean, Coq

%Section: Scholarships and additional info
\section{Awards and Accomplishments}
\begin{tabular*}{\textwidth}{l @{\extracolsep{\fill}} r}
	Provost's Scholar & \textsc{June} 2020 \\
	Prince of Wales Prize in Mathematics & \textsc{June} 2020 \\
	Dean's List Scholar & \textsc{April} 2020, 2019, 2018 \\
	University of Toronto Scholar & \textsc{December} 2018 \\
	Trinity College Chancellor's Scholarship (Ashbaugh Fund) & \textsc{November} 2018 \\
\end{tabular*}

\section{Experience}

\begin{tabular*}{\textwidth}{l @{\extracolsep{\fill}} r}
\textbf{Research Assistant} & \textsc{Current} \\
University of Cambridge \\
\multicolumn{2}{l}{\textit{Programming languages research as part of the TypeFoundry project, focusing on}}
\\ \textit{verified low-level compilers, optimizers, and software architecture.}
\\
\\
\textbf{Course Supervisor} & \textsc{Current} \\
University of Cambridge \\
\multicolumn{2}{l}{\textit{Supervised tutorials for Optimizing Compilers, Types, and Hoare Logic.}}
\\
\\ \textbf{Developer} & \textsc{Summer} 2022 \\
Canonical Alpha Capital Management \\
\multicolumn{2}{l}{\textit{Worked on developing trading strategies in Rust, contributed to the Barter library}}
\\
\\
\textbf{MAT135/MAT136 (Calculus II) Teaching Assistant} & \textsc{Winter} 2019 \\
University of Toronto \\
\multicolumn{2}{l}{\textit{Held tutorial sections and office hours, marked assignments, quizzes, and midterms}}
\end{tabular*}

\break

\section{Projects and Skills}

\begin{itemize}
	
	\item \textbf{Programming Languages}, especially as relates to the design of type systems. Currently working on \texttt{isotope}, a dependently typed low-level intermediate representation.

	\item \textbf{Compiler and Interpreter Development} in \textbf{C}, \textbf{C++} and \textbf{Rust}. Made multiple contributions to the official Rust compiler. Re-implemented CSC488's project (a compiler for a simple imperative language) in Rust as \texttt{refcomp} using LLVM. Wrote a preprocessor for the C11 language and part of a parser, \texttt{simplec}. Worked on low-level interpretation and representation of dependent and higher types in a prototype of \texttt{isotope} as part of my master's thesis.

	\item \textbf{Systems Development} in \textbf{C}, \textbf{C++} and \textbf{Rust}, including embedded systems in C. Experienced with memory management and concurrency. Some experience with server and kernel module development. Developed the \texttt{elysees}, \texttt{hayami}, \texttt{congruence}, and \texttt{typed-generational-arena} Rust packages listed on \verb|crates.io|, which I now actively maintain, implementing atomic reference counting, symbol tables, a suite of congruence closure algorithms for symbolic manipulation, and arena allocation respectively.
	
	\item \textbf{Formal Verification} using \textbf{Lean}, \textbf{Coq}, and \textbf{Agda}. Verified an experimental programming language's semantics in \textbf{Lean} as part of the Logical Refinement Types project, which is currently being submitted to OOPSLA'23. Familiarity with the \textbf{Calculus of Inductive Constructions} and variants thereof. Independently worked through a portion of the Software Foundations Coq development, and currently working on formalizing Logical Refinement Types in Lean.
	
	\item \textbf{Functional Programming} in \textbf{Haskell} and \textbf{Agda}. Significant experience with the \textbf{lambda calculus} and \textbf{type theory}, and the general theory of programming languages, especially as applied to compiler development.

	\item \textbf{Digital Circuit Design} in \textbf{Verilog}, using Quartus Prime, Verilator, and Icarus Verilog. Designed and implemented (on an Intel FPGA) a toy 16-bit CPU with support for a VGA display and a custom instruction set, \texttt{flow}. Helped extend it to a 32-bit version, \texttt{rapids}.

	\item \textbf{Game Development} in \textbf{Rust}, \textbf{Java}, and \textbf{C++}. Made significant contributions to a Rust Minecraft clone (\texttt{voxel-rs}) by adding physics and allowing the player to modify the game world.

	\item \textbf{Scientific Computing} in \textbf{Python}, \textbf{Julia}, \textbf{C}, \textbf{C++} and \textbf{CUDA}. Experience writing high-speed numerical algorithms, including optimized matrix multiplication, FFT, and a dense eigensolver.

	\item \textbf{Machine Learning} using \textbf{PyTorch}, \textbf{Knet}, \textbf{Tensorflow}, \textbf{Keras}, and \textbf{SciPy}. Worked on implementing a variety of machine learning algorithms in PyTorch, including Gated Graph Sequence Neural Networks and Equilibrium Propagation.

	\item \textbf{Typesetting} in \LaTeX, which I used to make comprehensive sets of notes for many of my courses at the University of Toronto available for free on my GitLab

	\item \textbf{Languages:} Fluent \textbf{English} and \textbf{French}, elementary \textbf{Arabic} proficiency. Have been studying \textbf{Mandarin} since the summer of 2019.
	
\end{itemize}

\end{document}
